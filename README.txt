A port of GNUBoy to OpenBSD / DRM framebuffer console

Requirements
------------
Nothing outside of the OpenBSD base install

Building
--------
$ make

Running
-------
Don't run bin/gnuboy just yet. Mostly because if the keyboard doesn't
work and you don't have a convenient SSH connection into your
machine, it will be very difficult to exit the program. Instead run:

$ bin/demo

You should see a tech demo. Press the up and down arrows to check
if the keyboard is working. The program will exit automatically
after a time. If the keyboard is working you can press the escape
key to exit early.

If the keyboard is not working, it is because by default it uses
the /dev/wskbd (MUX) device rather than i.e /dev/wskbd[0,1,2,3] and
unfortunately the mapping is only obtained for the first device
(/dev/wskbd0). In short, you must specify which device to use. For
example:

$ SYS_KEYBOARD=/dev/wskbd1 bin/demo

  or

$ SYS_KEYBOARD=/dev/wskbd2 bin/demo

Be aware that when you log in, /dev/wskbd and /dev/wskbd0 get
ownership assigned to you, however in order to grab the keymap for
/dev/wskbd[1, 2, 3], even though they are part of the MUX, you need
ownership / rw access which you will need to provide manually. For
example:

# chown <username>:<username> /dev/wskbd1

Once this is working, you can use that same environment variable
for bin/gnuboy:

$ bin/gnuboy roms/SuperClone.gb

or

$ SYS_KEYBOARD=/dev/wskbd1 bin/gnuboy roms/SpaceSamurai.gb

Controls
--------
Escape     - Exit
Arrow Keys - DPad
Enter      - Start
Tab        - Select
X, Z       - A, B
F          - Fullscreen
S          - Save state
L          - Load state

Development
-----------
If you change the project substantially, you can regenerate the
Makefile via:

$ awk -f configure.awk

If you do fix anything, please send any patches to me via
kpedersen@thamessoftware.co.uk

Hope you enjoy!

Karsten

