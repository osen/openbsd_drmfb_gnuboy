CXX=c++
CC=cc
AR=ar
CFLAGS=-O3 -Isrc -I/usr/X11R6/include -I/usr/X11R6/include/libdrm
CXXFLAGS=-Isrc
LFLAGS=-O3 -Llib -L/usr/X11R6/lib -ldrm

.POSIX:
.PHONY: clean
.SUFFIXES:
.SUFFIXES: .o .c .cpp

all: objs/.dirs lib/libgz.a lib/libsys.a lib/libxz.a bin/gnuboy bin/vsync bin/demo

bin/gnuboy: objs/gnuboy/mem.o objs/gnuboy/emu.o objs/gnuboy/sound.o objs/gnuboy/cpu.o objs/gnuboy/rcfile.o objs/gnuboy/exports.o objs/gnuboy/fastmem.o objs/gnuboy/hw.o objs/gnuboy/main.o objs/gnuboy/rtc.o objs/gnuboy/debug.o objs/gnuboy/save.o objs/gnuboy/io.o objs/gnuboy/keytable.o objs/gnuboy/palette.o objs/gnuboy/lcd.o objs/gnuboy/path.o objs/gnuboy/rccmds.o objs/gnuboy/lcdc.o objs/gnuboy/rckeys.o objs/gnuboy/split.o objs/gnuboy/events.o objs/gnuboy/sys.o objs/gnuboy/loader.o objs/gnuboy/rcvars.o objs/gnuboy/refresh.o lib/libgz.a lib/libsys.a lib/libxz.a
	$(CXX) -o$@ objs/gnuboy/mem.o objs/gnuboy/emu.o objs/gnuboy/sound.o objs/gnuboy/cpu.o objs/gnuboy/rcfile.o objs/gnuboy/exports.o objs/gnuboy/fastmem.o objs/gnuboy/hw.o objs/gnuboy/main.o objs/gnuboy/rtc.o objs/gnuboy/debug.o objs/gnuboy/save.o objs/gnuboy/io.o objs/gnuboy/keytable.o objs/gnuboy/palette.o objs/gnuboy/lcd.o objs/gnuboy/path.o objs/gnuboy/rccmds.o objs/gnuboy/lcdc.o objs/gnuboy/rckeys.o objs/gnuboy/split.o objs/gnuboy/events.o objs/gnuboy/sys.o objs/gnuboy/loader.o objs/gnuboy/rcvars.o objs/gnuboy/refresh.o $(LFLAGS) -lgz -lsys -lxz

lib/libgz.a: objs/gz/inflate.o 
	$(AR) rcs $@ objs/gz/inflate.o

lib/libsys.a: objs/sys/Screen.o objs/sys/Keyboard.o objs/sys/sys.o 
	$(AR) rcs $@ objs/sys/Screen.o objs/sys/Keyboard.o objs/sys/sys.o

bin/vsync: objs/vsync/main.o 
	$(CXX) -o$@ objs/vsync/main.o $(LFLAGS) 

lib/libxz.a: objs/xz/xz_dec_lzma2.o objs/xz/xz_dec_stream.o objs/xz/xz_crc32.o objs/xz/xz_crc64.o objs/xz/xz_dec_bcj.o 
	$(AR) rcs $@ objs/xz/xz_dec_lzma2.o objs/xz/xz_dec_stream.o objs/xz/xz_crc32.o objs/xz/xz_crc64.o objs/xz/xz_dec_bcj.o

bin/demo: objs/demo/main.o lib/libsys.a
	$(CXX) -o$@ objs/demo/main.o $(LFLAGS) -lsys

objs/gnuboy/mem.o: src/gnuboy/mem.c src/gnuboy/lcdc.h src/gnuboy/hw.h src/gnuboy/io.h src/gnuboy/sound.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/rtc.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/mem.c

objs/gz/inflate.o: src/gz/inflate.c 
	$(CC) $(CFLAGS) -c -o$@ src/gz/inflate.c

objs/sys/Screen.o: src/sys/Screen.c src/sys/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/sys/Screen.c

objs/gnuboy/emu.o: src/gnuboy/emu.c src/gnuboy/rc.h src/gnuboy/cpu.h src/gnuboy/hw.h src/gnuboy/io.h src/gnuboy/sound.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/rtc.h src/gnuboy/sys.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/emu.c

objs/sys/Keyboard.o: src/sys/Keyboard.c src/sys/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/sys/Keyboard.c

objs/gnuboy/sound.o: src/gnuboy/sound.c src/gnuboy/rc.h src/gnuboy/cpu.h src/gnuboy/pcm.h src/gnuboy/hw.h src/gnuboy/sound.h src/gnuboy/noise.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/sys.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/sound.c

objs/xz/xz_dec_lzma2.o: src/xz/xz_dec_lzma2.c src/xz/xz_private.h src/xz/xz.h src/xz/xz_lzma2.h src/xz/xz_config.h
	$(CC) $(CFLAGS) -c -o$@ src/xz/xz_dec_lzma2.c

objs/xz/xz_dec_stream.o: src/xz/xz_dec_stream.c src/xz/xz_private.h src/xz/xz_stream.h src/xz/xz.h src/xz/xz_config.h
	$(CC) $(CFLAGS) -c -o$@ src/xz/xz_dec_stream.c

objs/gnuboy/cpu.o: src/gnuboy/cpu.c src/gnuboy/lcdc.h src/gnuboy/cpucore.h src/gnuboy/debug.h src/gnuboy/cpuregs.h src/gnuboy/cpu.h src/gnuboy/hw.h src/gnuboy/fastmem.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/cpu.c

objs/gnuboy/rcfile.o: src/gnuboy/rcfile.c src/gnuboy/rc.h src/gnuboy/hw.h src/gnuboy/path.h src/gnuboy/defs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/rcfile.c

objs/gnuboy/exports.o: src/gnuboy/exports.c src/gnuboy/rc.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/exports.c

objs/demo/main.o: src/demo/main.c src/sys/sys.h src/gnuboy/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/demo/main.c

objs/gnuboy/fastmem.o: src/gnuboy/fastmem.c src/gnuboy/fastmem.h src/gnuboy/mem.h src/gnuboy/defs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/fastmem.c

objs/gnuboy/hw.o: src/gnuboy/hw.c src/gnuboy/cpu.h src/gnuboy/hw.h src/gnuboy/fastmem.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/hw.c

objs/gnuboy/main.o: src/gnuboy/main.c src/gnuboy/rc.h src/gnuboy/input.h src/gnuboy/io.h src/gnuboy/exports.h src/gnuboy/emu.h src/gnuboy/loader.h src/gnuboy/rckeys.h src/gnuboy/version.h src/gnuboy/defs.h src/gnuboy/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/main.c

objs/gnuboy/rtc.o: src/gnuboy/rtc.c src/gnuboy/rc.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/rtc.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/rtc.c

objs/gnuboy/debug.o: src/gnuboy/debug.c src/gnuboy/rc.h src/gnuboy/cpuregs.h src/gnuboy/cpu.h src/gnuboy/fastmem.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/debug.c

objs/gnuboy/save.o: src/gnuboy/save.c src/gnuboy/cpuregs.h src/gnuboy/cpu.h src/gnuboy/hw.h src/gnuboy/sound.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/rtc.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/save.c

objs/gnuboy/io.o: src/gnuboy/io.c src/gnuboy/debug.h src/gnuboy/io.h src/gnuboy/defs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/io.c

objs/sys/sys.o: src/sys/sys.c src/sys/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/sys/sys.c

objs/vsync/main.o: src/vsync/main.c 
	$(CC) $(CFLAGS) -c -o$@ src/vsync/main.c

objs/gnuboy/keytable.o: src/gnuboy/keytable.c src/gnuboy/input.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/keytable.c

objs/xz/xz_crc32.o: src/xz/xz_crc32.c src/xz/xz_private.h src/xz/xz.h src/xz/xz_config.h
	$(CC) $(CFLAGS) -c -o$@ src/xz/xz_crc32.c

objs/gnuboy/palette.o: src/gnuboy/palette.c src/gnuboy/fb.h src/gnuboy/defs.h src/gnuboy/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/palette.c

objs/gnuboy/lcd.o: src/gnuboy/lcd.c src/gnuboy/fb.h src/gnuboy/rc.h src/gnuboy/hw.h src/gnuboy/palette.h src/gnuboy/refresh.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/lcd.c

objs/xz/xz_crc64.o: src/xz/xz_crc64.c src/xz/xz_private.h src/xz/xz.h src/xz/xz_config.h
	$(CC) $(CFLAGS) -c -o$@ src/xz/xz_crc64.c

objs/gnuboy/path.o: src/gnuboy/path.c 
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/path.c

objs/gnuboy/rccmds.o: src/gnuboy/rccmds.c src/gnuboy/rc.h src/gnuboy/split.h src/gnuboy/hw.h src/gnuboy/emu.h src/gnuboy/rckeys.h src/gnuboy/loader.h src/gnuboy/defs.h src/gnuboy/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/rccmds.c

objs/xz/xz_dec_bcj.o: src/xz/xz_dec_bcj.c src/xz/xz_private.h src/xz/xz.h src/xz/xz_config.h
	$(CC) $(CFLAGS) -c -o$@ src/xz/xz_dec_bcj.c

objs/gnuboy/lcdc.o: src/gnuboy/lcdc.c src/gnuboy/cpu.h src/gnuboy/hw.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/lcdc.c

objs/gnuboy/rckeys.o: src/gnuboy/rckeys.c src/gnuboy/rc.h src/gnuboy/input.h src/gnuboy/defs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/rckeys.c

objs/gnuboy/split.o: src/gnuboy/split.c 
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/split.c

objs/gnuboy/events.o: src/gnuboy/events.c src/gnuboy/input.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/events.c

objs/gnuboy/sys.o: src/gnuboy/sys.c src/gnuboy/fb.h src/gnuboy/rc.h src/sys/sys.h src/gnuboy/input.h src/gnuboy/pcm.h src/gnuboy/defs.h src/gnuboy/sys.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/sys.c

objs/gnuboy/loader.o: src/gnuboy/loader.c src/gnuboy/rc.h src/gz/inflate.h src/gnuboy/hw.h src/xz/xz.h src/gnuboy/sound.h src/gnuboy/save.h src/gnuboy/lcd.h src/gnuboy/mem.h src/gnuboy/defs.h src/gnuboy/rtc.h src/gnuboy/sys.h src/xz/xz_config.h src/gnuboy/regs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/loader.c

objs/gnuboy/rcvars.o: src/gnuboy/rcvars.c src/gnuboy/rc.h src/gnuboy/defs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/rcvars.c

objs/gnuboy/refresh.o: src/gnuboy/refresh.c src/gnuboy/lcd.h src/gnuboy/defs.h
	$(CC) $(CFLAGS) -c -o$@ src/gnuboy/refresh.c

objs/.dirs:
	mkdir bin
	mkdir lib
	mkdir objs
	mkdir objs/gnuboy
	mkdir objs/gz
	mkdir objs/sys
	mkdir objs/vsync
	mkdir objs/xz
	mkdir objs/demo
	@touch $@

clean:
	rm -r -f bin
	rm -r -f lib
	rm -r -f objs

