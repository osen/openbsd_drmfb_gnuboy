#include "sys.h"

#include <xf86drm.h>
#include <xf86drmMode.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/select.h>

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int flipRequested;

struct Screen
{
  int fd;
  unsigned char *pixels;
  int width;
  int height;

  struct modeset_dev *devs;
  struct modeset_dev *dev;
};

struct modeset_buf
{
  uint32_t width;
  uint32_t height;
  uint32_t stride;
  uint32_t size;
  uint32_t handle;
  uint8_t *map;
  uint32_t fb;
};

struct modeset_dev
{
  struct modeset_dev *next;

  unsigned int front_buf;
  struct modeset_buf bufs[2];

  drmModeModeInfo mode;
  uint32_t conn;
  uint32_t crtc;
  drmModeCrtc *saved_crtc;
};

static struct Screen screen;

static int
modeset_open(int *out, const char *node)
{
  int fd, ret;
  uint64_t has_dumb;

  fd = open(node, O_RDWR | O_CLOEXEC);

  if(fd < 0)
  {
    ret = -errno;
    fprintf(stderr, "cannot open '%s': %m\n", node);

    return ret;
  }

  if(drmGetCap(fd, DRM_CAP_DUMB_BUFFER, &has_dumb) < 0 || !has_dumb)
  {
    fprintf(stderr, "drm device '%s' does not support dumb buffers\n", node);
    close(fd);

    return -EOPNOTSUPP;
  }

  *out = fd;

  return 0;
}


static int
modeset_find_crtc(int fd, drmModeRes *res, drmModeConnector *conn,
  struct modeset_dev *dev)
{
  drmModeEncoder *enc;
  unsigned int i, j;
  int32_t crtc;
  struct modeset_dev *iter;

  /* first try the currently connected encoder+crtc */
  if(conn->encoder_id)
  {
    enc = drmModeGetEncoder(fd, conn->encoder_id);
  }
  else
  {
    enc = NULL;
  }

  if(enc)
  {
    if(enc->crtc_id)
    {
      crtc = enc->crtc_id;

      for(iter = screen.devs; iter; iter = iter->next)
      {
        if(iter->crtc == crtc)
        {
          crtc = -1;
          break;
        }
      }

      if(crtc >= 0)
      {
        drmModeFreeEncoder(enc);
        dev->crtc = crtc;
        return 0;
      }
    }

    drmModeFreeEncoder(enc);
  }

  /*
   * If the connector is not currently bound to an encoder or if the
   * encoder+crtc is already used by another connector (actually unlikely
   * but lets be safe), iterate all other available encoders to find a
   * matching CRTC.
   */
  for(i = 0; i < conn->count_encoders; ++i)
  {
    enc = drmModeGetEncoder(fd, conn->encoders[i]);

    if(!enc)
    {
      fprintf(stderr, "cannot retrieve encoder %u:%u (%d): %m\n",
        i, conn->encoders[i], errno);

      continue;
    }

    /* iterate all global CRTCs */
    for(j = 0; j < res->count_crtcs; ++j)
    {
      /* check whether this CRTC works with the encoder */
      if (!(enc->possible_crtcs & (1 << j)))
      {
        continue;
      }

      /* check that no other device already uses this CRTC */
      crtc = res->crtcs[j];

      for(iter = screen.devs; iter; iter = iter->next)
      {
        if(iter->crtc == crtc)
        {
          crtc = -1;
          break;
        }
      }

      /* we have found a CRTC, so save it and return */
      if(crtc >= 0)
      {
        drmModeFreeEncoder(enc);
        dev->crtc = crtc;
        return 0;
      }
    }

    drmModeFreeEncoder(enc);
  }

  fprintf(stderr, "cannot find suitable CRTC for connector %u\n",
    conn->connector_id);

  return -ENOENT;
}

static int
modeset_create_fb(int fd, struct modeset_buf *buf)
{
  struct drm_mode_create_dumb creq;
  struct drm_mode_destroy_dumb dreq;
  struct drm_mode_map_dumb mreq;
  int ret;

  /* create dumb buffer */
  memset(&creq, 0, sizeof(creq));
  creq.width = buf->width;
  creq.height = buf->height;
  creq.bpp = 32;
  ret = drmIoctl(fd, DRM_IOCTL_MODE_CREATE_DUMB, &creq);

  if(ret < 0)
  {
    fprintf(stderr, "cannot create dumb buffer (%d): %m\n", errno);

    return -errno;
  }

  buf->stride = creq.pitch;
  buf->size = creq.size;
  buf->handle = creq.handle;

  /* create framebuffer object for the dumb-buffer */
  ret = drmModeAddFB(fd, buf->width, buf->height, 24, 32, buf->stride,
    buf->handle, &buf->fb);

  if(ret)
  {
    fprintf(stderr, "cannot create framebuffer (%d): %m\n", errno);
    ret = -errno;

    goto err_destroy;
  }

  /* prepare buffer for memory mapping */
  memset(&mreq, 0, sizeof(mreq));
  mreq.handle = buf->handle;
  ret = drmIoctl(fd, DRM_IOCTL_MODE_MAP_DUMB, &mreq);

  if(ret)
  {
    fprintf(stderr, "cannot map dumb buffer (%d): %m\n", errno);
    ret = -errno;

    goto err_fb;
  }

  /* perform actual memory mapping */
  buf->map = mmap(0, buf->size, PROT_READ | PROT_WRITE, MAP_SHARED,
    fd, mreq.offset);

  if(buf->map == MAP_FAILED)
  {
    fprintf(stderr, "cannot mmap dumb buffer (%d): %m\n", errno);
    ret = -errno;

    goto err_fb;
  }

  /* clear the framebuffer to 0 */
  memset(buf->map, 0, buf->size);

  return 0;

err_fb:
  drmModeRmFB(fd, buf->fb);

err_destroy:
  memset(&dreq, 0, sizeof(dreq));
  dreq.handle = buf->handle;
  drmIoctl(fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dreq);

  return ret;
}

static void
modeset_destroy_fb(int fd, struct modeset_buf *buf)
{
  struct drm_mode_destroy_dumb dreq;

  /* unmap buffer */
  munmap(buf->map, buf->size);

  /* delete framebuffer */
  drmModeRmFB(fd, buf->fb);

  /* delete dumb buffer */
  memset(&dreq, 0, sizeof(dreq));
  dreq.handle = buf->handle;
  drmIoctl(fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dreq);
}

static int
modeset_setup_dev(struct Screen *scr, int fd, drmModeRes *res, drmModeConnector *conn,
  struct modeset_dev *dev)
{
  int ret;

  /* check if a monitor is connected */
  if(conn->connection != DRM_MODE_CONNECTED)
  {
    fprintf(stderr, "ignoring unused connector %u\n",
      conn->connector_id);

    return -ENOENT;
  }

  /* check if there is at least one valid mode */
  if(conn->count_modes == 0)
  {
    fprintf(stderr, "no valid mode for connector %u\n",
      conn->connector_id);

    return -EFAULT;
  }

  /*
   * copy the mode information into our device structure and into both
   * buffers
   */
  memcpy(&dev->mode, &conn->modes[0], sizeof(dev->mode));
  dev->bufs[0].width = conn->modes[0].hdisplay;
  dev->bufs[0].height = conn->modes[0].vdisplay;
  dev->bufs[1].width = conn->modes[0].hdisplay;
  dev->bufs[1].height = conn->modes[0].vdisplay;

  fprintf(stderr, "mode for connector %u is %ux%u\n",
    conn->connector_id, dev->bufs[0].width, dev->bufs[0].height);

  /* find a crtc for this connector */
  ret = modeset_find_crtc(fd, res, conn, dev);

  if(ret)
  {
    fprintf(stderr, "no valid crtc for connector %u\n",
      conn->connector_id);

    return ret;
  }

  /* create framebuffer #1 for this CRTC */
  ret = modeset_create_fb(fd, &dev->bufs[0]);

  if(ret)
  {
    fprintf(stderr, "cannot create framebuffer for connector %u\n",
      conn->connector_id);

    return ret;
  }

  /* create framebuffer #2 for this CRTC */
  ret = modeset_create_fb(fd, &dev->bufs[1]);

  if(ret)
  {
    fprintf(stderr, "cannot create framebuffer for connector %u\n",
      conn->connector_id);

    modeset_destroy_fb(fd, &dev->bufs[0]);

    return ret;
  }

  return 0;
}

static int
modeset_prepare(struct Screen* scr, int fd)
{
  drmModeRes *res;
  drmModeConnector *conn;
  unsigned int i;
  struct modeset_dev *dev;
  int ret;
  struct modeset_dev *iter;
  struct modeset_buf *buf;

  /* retrieve resources */
  res = drmModeGetResources(fd);

  if(!res)
  {
    fprintf(stderr, "cannot retrieve DRM resources (%d): %m\n", errno);

    return -errno;
  }

  /* iterate all connectors */
  for(i = 0; i < res->count_connectors; ++i)
  {
    /* get information for each connector */
    conn = drmModeGetConnector(fd, res->connectors[i]);

    if(!conn)
    {
      fprintf(stderr, "cannot retrieve DRM connector %u:%u (%d): %m\n",
        i, res->connectors[i], errno);

      continue;
    }

    /* create a device structure */
    dev = malloc(sizeof(*dev));
    memset(dev, 0, sizeof(*dev));
    dev->conn = conn->connector_id;

    /* call helper function to prepare this connector */
    ret = modeset_setup_dev(scr, fd, res, conn, dev);

    if(ret)
    {
      if(ret != -ENOENT)
      {
        errno = -ret;
        fprintf(stderr, "cannot setup device for connector %u:%u (%d): %m\n",
          i, res->connectors[i], errno);
      }

      free(dev);
      drmModeFreeConnector(conn);

      continue;
    }

    /* free connector data and link device into global list */
    drmModeFreeConnector(conn);
    dev->next = screen.devs;
    screen.devs = dev;
  }

  /* free resources again */
  drmModeFreeResources(res);

  /* perform actual modesetting on each found connector+CRTC */
  for(iter = screen.devs; iter; iter = iter->next)
  {
    iter->saved_crtc = drmModeGetCrtc(screen.fd, iter->crtc);
    buf = &iter->bufs[iter->front_buf];

    ret = drmModeSetCrtc(screen.fd, iter->crtc, buf->fb, 0, 0,
      &iter->conn, 1, &iter->mode);

    if(ret)
    {
      fprintf(stderr, "cannot set CRTC for connector %u (%d): %m\n",
        iter->conn, errno);
    }
  }

  return 0;
}

static void
modeset_wait_flip(int fd)
{
  int ret;
  fd_set fds;
  drmEventContext ev = {0};
  int eventFound = 0;

  if(!flipRequested)
  {
    return;
  }

  FD_ZERO(&fds);
  FD_SET(fd, &fds);

  ret = select(fd + 1, &fds, NULL, NULL, NULL);

  if(ret < 0)
  {
    fprintf(stderr, "select() failed with %d: %m\n", errno);
  }
  else if(FD_ISSET(fd, &fds))
  {
    drmHandleEvent(fd, &ev);
    eventFound = 1;
  }
  else
  {
    abort();
  }

  if(!eventFound) return;

  while(1)
  {
    struct timeval v = {0};
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    ret = select(fd + 1, &fds, NULL, NULL, &v);

    if(ret < 0)
    {
      fprintf(stderr, "select() failed with %d: %m\n", errno);
    }
    else if(FD_ISSET(fd, &fds))
    {
      drmHandleEvent(fd, &ev);
    }
    else
    {
      break;
    }
  }
}

static void
modeset_swap_dev(int fd, struct modeset_dev *dev)
{
  struct modeset_buf *buf;
  int ret;

/*
  modeset_wait_flip(fd);
  buf = &dev->bufs[dev->front_buf ^ 1];
*/

  buf = &dev->bufs[dev->front_buf ^ 1];

  ret = drmModePageFlip(fd, dev->crtc, buf->fb,
    DRM_MODE_PAGE_FLIP_EVENT, dev);

  if(ret)
  {
    fprintf(stderr, "cannot flip CRTC for connector %u (%d): %m\n",
      dev->conn, errno);
  }
  else
  {
    flipRequested = 1;
    modeset_wait_flip(fd);
    dev->front_buf ^= 1;
  }
}

static void
modeset_cleanup(int fd)
{
  struct modeset_dev *iter;
  int ret;

  //modeset_wait_flip(fd);

  while(screen.devs)
  {
    /* remove from global list */
    iter = screen.devs;
    screen.devs = iter->next;

    /* restore saved CRTC configuration */
    drmModeSetCrtc(fd,
      iter->saved_crtc->crtc_id,
      iter->saved_crtc->buffer_id,
      iter->saved_crtc->x,
      iter->saved_crtc->y,
      &iter->conn,
      1,
      &iter->saved_crtc->mode);

    drmModeFreeCrtc(iter->saved_crtc);

    /* destroy framebuffers */
    modeset_destroy_fb(fd, &iter->bufs[1]);
    modeset_destroy_fb(fd, &iter->bufs[0]);

    /* free allocated memory */
    free(iter);
  }
}

void
_SysScreenInit()
{
  int ret = 0;

  ret = modeset_open(&screen.fd, "/dev/dri/card0");
  if(ret) abort();

  /* prepare all connectors and CRTCs */
  ret = modeset_prepare(&screen, screen.fd);
  if(ret) abort();

  screen.dev = screen.devs;
  screen.width = screen.dev->bufs[0].width;
  screen.height = screen.dev->bufs[0].height;

  screen.pixels = calloc(screen.width * screen.height * 3,
    sizeof(*screen.pixels));

  printf("Screen size: %ix%i\n", screen.width, screen.height);
}

void
_SysScreenCleanup()
{
  free(screen.pixels);
  modeset_cleanup(screen.fd);
  close(screen.fd);
}

void
_SysScreenUpdate()
{
  modeset_swap_dev(screen.fd, screen.dev);
}

void
SysScreenSetPixel(int x, int y,
  unsigned char r, unsigned char g, unsigned char b)
{
  struct modeset_dev *dev = screen.dev;

  if(x < 0 || x >= screen.width || y < 0 || y >= screen.height) return;

  struct modeset_buf *buf = &dev->bufs[dev->front_buf ^ 1];

  unsigned int off = buf->stride * y + x * 4;

  *(uint32_t*)&buf->map[off] =
    (r << 16) | (g << 8) | b;
}

void
SysScreenDrawRect(int x, int y, int w, int h,
  unsigned char r, unsigned char g, unsigned char b)
{
  {int yi = y; for(; yi < y + h; ++yi)
  {
    {int xi = x; for(; xi < x + w; ++xi)
    {
      SysScreenSetPixel(xi, yi, r, g, b);
    }}
  }}
}

int
SysScreenWidth()
{
  return screen.width;
}

int
SysScreenHeight()
{
  return screen.height;
}

void
SysScreenClear(unsigned char r, unsigned char g, unsigned char b)
{
  SysScreenDrawRect(0, 0, SysScreenWidth(), SysScreenHeight(), r, g, b);
  _SysScreenUpdate();
  SysScreenDrawRect(0, 0, SysScreenWidth(), SysScreenHeight(), r, g, b);
  _SysScreenUpdate();
}

