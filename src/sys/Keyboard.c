#include "sys.h"

#include <sys/time.h>
#include <sys/ioctl.h>
#include <dev/wscons/wsconsio.h>
//#include <dev/wscons/wsksymvar.h>
//#include <dev/wscons/wsksymdef.h>

#include <unistd.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Keyboard
{
  int fd;
  struct wscons_keymap map[KS_NUMKEYCODES];
  struct wskbd_map_data keymap;

  int keys[512];
  int downKeys[512];
  int upKeys[512];
};

static struct Keyboard keyboard;

static void
list_devices(int fd)
{
  int i, rfd;
  char buf[100];
  struct wsmux_device_list devs;
  const char *name;

  if(ioctl(fd, WSMUXIO_LIST_DEVICES, &devs) < 0)
  {
    SysPanic("WSMUXIO_LIST_DEVICES");
  }

  for(i = 0; i < devs.ndevices; i++)
  {
    if(devs.devices[i].type == WSMUX_KBD)
    {
      printf("Kbd %i\n", devs.devices[i].idx);
    }
    else if(devs.devices[i].type == WSMUX_MOUSE)
    {
      printf("Mouse\n");
    }
    else if(devs.devices[i].type == WSMUX_MUX)
    {
      printf("Mux\n");
    }
  }
}

void
_SysKeyboardInit()
{
  const char *dev = NULL;

  dev = getenv("SYS_KEYBOARD");

  if(!dev)
  {
    dev = "/dev/wskbd";
  }

  /* Open up the keyboard device to obtain keymap */

  int fd = open(dev, O_RDWR | O_NONBLOCK | O_CLOEXEC);

  if(fd == -1)
  {
    SysPanic("Failed to open keyboard device");
  }

  keyboard.keymap.maplen = KS_NUMKEYCODES;
  keyboard.keymap.map = keyboard.map;
  ioctl(fd, WSKBDIO_GETMAP, &keyboard.keymap);

  close(fd);

  /* Open up the keyboard MUX device for reading keys */

  keyboard.fd = open("/dev/wskbd", O_RDWR | O_NONBLOCK | O_CLOEXEC);

  if(keyboard.fd == -1)
  {
    SysPanic("Failed to open keyboard device");
  }

  //list_devices(keyboard.fd);
}

void
_SysKeyboardCleanup()
{
  close(keyboard.fd);
}

void
_SysKeyboardUpdate()
{
  struct wscons_event events[64];
  int n;
  int type;
  int value;

  memset(keyboard.downKeys, 0, sizeof(keyboard.downKeys));
  memset(keyboard.upKeys, 0, sizeof(keyboard.upKeys));

  n = read(keyboard.fd, events, sizeof(events));
  if(n < 1) return;
  n /= sizeof(struct wscons_event);

  {int ei = 0; for(; ei < n; ++ei)
  {
    type = events[ei].type;

    if(type == WSCONS_EVENT_KEY_DOWN)
    {
      value = events[ei].value;
      value = keyboard.keymap.map[events[ei].value].group1[0];

      /* DEBUG */
      /*
      printf("%i\n", value);
      if(value == KS_a) abort();
      */

      //if(value >= 512) continue;
      keyboard.keys[value % 512] = 1;
      keyboard.downKeys[value % 512] = 1;
    }
    else if(type == WSCONS_EVENT_KEY_UP)
    {
      value = events[ei].value;
      value = keyboard.keymap.map[events[ei].value].group1[0];

      //if(value >= 512) continue;
      keyboard.keys[value % 512] = 0;
      keyboard.upKeys[value % 512] = 1;
    }
  }}
}

int
SysKeyboardKey(int key)
{
  //if(key >= 512) return 0;
  return keyboard.keys[key % 512];
}

int
SysKeyboardDown(int key)
{
  return keyboard.downKeys[key % 512];
}

int
SysKeyboardUp(int key)
{
  return keyboard.upKeys[key % 512];
}

