#include <dev/wscons/wsksymdef.h>

void
SysInit();

void
SysCleanup();

void
SysUpdate();

void
SysSleep(int milli);

void
SysPanic(const char *message);


int
SysScreenWidth();

int
SysScreenHeight();

void
SysScreenClear(unsigned char r, unsigned char g, unsigned char b);

void
SysScreenSetPixel(int x, int y,
  unsigned char r, unsigned char g, unsigned char b);

void
SysScreenDrawRect(int x, int y, int w, int h,
  unsigned char r, unsigned char g, unsigned char b);

#define SYS_UP KS_Up
#define SYS_DOWN KS_Down
#define SYS_LEFT KS_Left
#define SYS_RIGHT KS_Right
#define SYS_a KS_a
#define SYS_s KS_s
#define SYS_l KS_l
#define SYS_x KS_x
#define SYS_z KS_z
#define SYS_f KS_f
#define SYS_ESCAPE KS_Escape
#define SYS_ENTER KS_Return
#define SYS_TAB KS_Tab

int
SysKeyboardKey(int key);

int
SysKeyboardDown(int key);

int
SysKeyboardUp(int key);


void
_SysScreenInit();

void
_SysScreenCleanup();

void
_SysScreenUpdate();

void
_SysKeyboardInit();

void
_SysKeyboardCleanup();

void
_SysKeyboardUpdate();

