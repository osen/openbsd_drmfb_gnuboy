#include "sys.h"

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>

void
SysInit()
{
  _SysScreenInit();
  _SysKeyboardInit();
}

void
SysCleanup()
{
  _SysScreenCleanup();
  _SysKeyboardCleanup();
}

void
SysUpdate()
{
  _SysScreenUpdate();
  _SysKeyboardUpdate();
}

void
SysSleep(int milli)
{
  usleep(1000 * milli);
}

void
SysPanic(const char *message)
{
  printf("Panic: %s\n", message);

  abort();
}

