#include "fb.h"
#include "pcm.h"
#include "rc.h"
#include "input.h"

#include <sys/sys.h>
#include <sys/time.h>

#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>

struct fb fb;
struct pcm pcm;

static int sound = 0;
static int stereo = 1;
static int samplerate = 44100;

rcvar_t pcm_exports[] =
{
  RCV_BOOL("sound", &sound),
  RCV_INT("stereo", &stereo),
  RCV_INT("samplerate", &samplerate),
  RCV_END
};

static int fullscreen = 0;
static int render_type = 0;
static int render_trace = 0;
static int vmode[3] = {0, 0, 32};

rcvar_t vid_exports[] =
{
  RCV_VECTOR("vmode", &vmode, 3),
  RCV_BOOL("fullscreen", &fullscreen),
  RCV_BOOL("vid_trace", &render_trace),
  RCV_INT("render_type", &render_type),
  RCV_END
};

static int joy_enable = 1;
static int joy_rumble_strength = 100;
static int joy_deadzone = 40;
static int alert = 0;
static int altenter = 0;
static int joy_trace = 0;

rcvar_t joy_exports[] =
{
  RCV_BOOL("joy", &joy_enable),
  RCV_INT("joy_rumble_strength", &joy_rumble_strength),
  RCV_INT("joy_deadzone", &joy_deadzone),
  RCV_INT("alert_on_quit", &alert),
  RCV_INT("altenter", &altenter),
  RCV_INT("joy_trace", &joy_trace),
  RCV_END
};

void vid_preinit() { }

char pixels[160 * 144 * 4];

void vid_init()
{
  SysInit();

  SysScreenClear(0, 0, 100);

  fb.ptr = pixels;
  fb.enabled = 1;
  fb.w = 160;
  fb.h = 144;
  fb.pelsize = 4;
  fb.pitch = fb.w * fb.pelsize;
  fb.cc[0].r = 0;
  fb.cc[1].r = 0;
  fb.cc[2].r = 0;
  fb.cc[3].r = 0;
  fb.cc[0].l = 16;
  fb.cc[1].l = 8;
  fb.cc[2].l = 0;
  fb.cc[3].l = 0;

  vmode[0] = fb.w;
  vmode[1] = fb.h;
}

void vid_begin() { }

void vid_end()
{
/*
  int x = 0;
  int y = 0;
  int p = 0;

  for(y = 0; y < vmode[1]; ++y)
  {
    for(x = 0; x < vmode[0]; ++x)
    {
      p = vmode[0] * 4 * y + x * 4;
      SysScreenSetPixel(x, y, pixels[p+0], pixels[p+1], pixels[p+2]);
    }
  }
*/

  int w = 320;
  int h = 240;
  int x = 0;
  int y = 0;
  int sx = 0;
  int sy = 0;
  float sf = 0;
  int p = 0;

  if(fullscreen)
  {
    w = SysScreenWidth();
    h = SysScreenHeight();
  }

  for(y = 0; y < h; ++y)
  {
    for(x = 0; x < w; ++x)
    {
      sf = (float)x / (float)w;
      sx = fb.w * sf;
      sf = (float)y / (float)h;
      sy = fb.h * sf;
      p = fb.w * 4 * sy + sx * 4;
      SysScreenSetPixel(x, y, pixels[p+2], pixels[p+1], pixels[p+0]);
    }
  }

  fb.dirty = 0;
}

void vid_close() { }
void vid_setpal(int i, int r, int g, int b) { }
void vid_settitle(char *title) { }

/* Stuff specific to sdl2-gnuboy video */
void vid_fullscreen_toggle() { }
void vid_screenshot() { }

void pcm_init() { }
int pcm_submit() { return 0; }
void pcm_close() { }
void pcm_pause() { }
void pcm_resume() { }

#define HANDLE_KEY(H, T) \
  if(SysKeyboardDown(H)) \
  { \
    ev.type = EV_PRESS; \
    ev.code = T; \
    ev_postevent(&ev); \
  } \
  if(SysKeyboardUp(H)) \
  { \
    ev.type = EV_RELEASE; \
    ev.code = T; \
    ev_postevent(&ev); \
  }

void ev_poll()
{
  if(SysKeyboardKey(SYS_ESCAPE))
  {
    SysCleanup();
    exit(0);
  }

  if(SysKeyboardDown(SYS_f))
  {
    fullscreen = !fullscreen;

    if(!fullscreen)
    {
      SysScreenClear(0, 0, 100);
    }
  }

  static int down = 0;
  event_t ev = {0};

  HANDLE_KEY(SYS_ENTER, K_ENTER)
  HANDLE_KEY(SYS_TAB, K_TAB)
  HANDLE_KEY(SYS_LEFT, K_LEFT)
  HANDLE_KEY(SYS_RIGHT, K_RIGHT)
  HANDLE_KEY(SYS_UP, K_UP)
  HANDLE_KEY(SYS_DOWN, K_DOWN)
  HANDLE_KEY(SYS_x, 'q')
  HANDLE_KEY(SYS_z, 'e')

  HANDLE_KEY(SYS_s, K_INS)
  HANDLE_KEY(SYS_l, K_DEL)

/*
  if(!down)
  {
    ev.type = EV_PRESS;
    ev.code = K_ENTER;
    ev_postevent(&ev);
  }
  else
  {
    ev.type = EV_RELEASE;
    ev.code = K_ENTER;
    ev_postevent(&ev);
  }

  down = !down;
*/

/*
  static int timer = 0;
  ++timer;
  if(timer > 1000) exit(0);
*/

  SysUpdate();
}

int confirm_exit() { return 0; }

void sys_checkdir(char *path, int wr)
{
  printf("sys_checkdir [%s] [%i]\n", path, wr);
}

void sys_sanitize(char *s) { }

void joy_init() { }
void joy_poll() { }
void joy_close() { }

void kb_init() { }
void kb_poll() { }
void kb_close() { }

int io_setup(const char* link) { return 0; }

static unsigned long timer;

void *sys_timer()
{
  struct timeval tv = {0};

  gettimeofday(&tv, NULL);
  timer = 1000000 * tv.tv_sec + tv.tv_usec;

  return &timer;
}

int sys_elapsed(int *cl)
{
  struct timeval tv = {0};
  unsigned long now = 0;
  int rtn = 0;

  gettimeofday(&tv, NULL);
  now = 1000000 * tv.tv_sec + tv.tv_usec;
  rtn = now - timer;
  timer = now;

  return rtn;
}

/*
void *sys_timer()
{
	Uint32 *tv;
	
	tv = malloc(sizeof *tv);
	*tv = SDL_GetTicks() * 1000;
	return tv;
}

int sys_elapsed(Uint32 *cl)
{
	Uint32 now;
	Uint32 usecs;

	now = SDL_GetTicks() * 1000;
	usecs = now - *cl;
	*cl = now;
	return usecs;
}
*/

void sys_sleep(int us)
{
	if(us <= 0) return;

	usleep(us);
}

void sys_initpath()
{
    char *buf = ".";

    if (rc_getstr("rcpath") == NULL)
        rc_setvar("rcpath", 1, &buf);

    if (rc_getstr("savedir") == NULL)
        rc_setvar("savedir", 1, &buf);
}

