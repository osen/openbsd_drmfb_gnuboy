#include <sys/sys.h>

#include <stdlib.h>

int main()
{
  SysInit();

  SysScreenClear(0, 0, 200);

  float x = 0;
  float y = 100;

  while(1)
  {
    if(SysKeyboardKey(SYS_ESCAPE)) break;

    x += 1.00f;
    if(x > 700) break;

    if(SysKeyboardKey(SYS_DOWN)) y++;
    if(SysKeyboardKey(SYS_UP)) y--;

    SysScreenDrawRect(0, 100, SysScreenWidth(), 10, 0, 0, 200);
    SysScreenDrawRect(0, 100, SysScreenWidth(), 10, 0, 200, 0);

    SysScreenDrawRect(x, y, 10, 10, 255, 0, 0);

    SysScreenDrawRect(0, 200, SysScreenWidth(), 10, 255, 0, 0);
    SysScreenDrawRect(0, 300, SysScreenWidth(), 10, 0, 255, 0);
    SysScreenDrawRect(0, 400, SysScreenWidth(), 10, 0, 0, 255);
    SysScreenDrawRect(0, 500, SysScreenWidth(), 10, 255, 0, 255);

    if(SysKeyboardKey(SYS_UP))
    {
      SysScreenDrawRect(0, 500, SysScreenWidth(), 10, 255, 255, 255);
    }

    SysUpdate();
    //SysSleep(16);
  }

  SysCleanup();

  return 0;
}

