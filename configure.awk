############################################################################
# configure.awk
#
# - Karsten Pedersen
#
# Generate a Makefile based on directory layouts rather than config files.
#
# - Create directories in src/ to add new executable or library modules.
# - Any module directory containing main.c[pp] will be an executable.
# - Link library dependencies found based on inter-module header inclusion.
############################################################################

function main()
{
  environment();
  find_targets()
  find_headers()
  find_source()
  MakefileOutput()
}

function environment()
{
  TYPE_EXECUTABLE=1
  TYPE_LIBRARY=2

  TYPE_RELATIVE=3
  TYPE_SYSTEM=4

  SYS_POSIX=5
  SYS_WIN32=6

  SYSTEM=SYS_POSIX
  PATH_SEP="/"
  OBJ_SUFFIX=".o"
  LIB_SUFFIX=".a"
  BIN_SUFFIX=""
  LIB_PREFIX="lib"

  #SYSTEM=SYS_WIN32
  #PATH_SEP="\\"
  #OBJ_SUFFIX=".obj"
  #LIB_SUFFIX=".lib"
  #BIN_SUFFIX=".exe"
  #LIB_PREFIX=""
}

function find_targets(dir,    cmd, line)
{
  if(SYSTEM == SYS_POSIX)
  {
    cmd = "ls src"
  }
  else if(SYSTEM == SYS_WIN32)
  {
    cmd = "cmd /C dir /B src"
  }
  else
  {
    error("Invalid platform")
  }

  while(cmd | getline line > 0)
  {
    Target(line)
  }

  close(cmd)
}

function find_headers(    cmd, line, targets, t)
{
  for(t in Targets)
  {
    cmd = "find src/" TargetName[t] " -name '*.h'"

    while(cmd | getline line > 0)
    {
      Header(t, line)
    }

    close(cmd)
  }
}

function find_source(    t, cmd, line, s)
{
  for(t in Targets)
  {
    cmd = "find src/" TargetName[t] " -name '*.c*'"

    while(cmd | getline line > 0)
    {
      s = Src(t, line)

      if(SrcName[s] == "main.cpp" || SrcName[s] == "main.c")
      {
        TargetSetType(t, TYPE_EXECUTABLE)
      }
    }

    close(cmd)
  }
}

############################################################################
# Src
############################################################################

function Src(target, path,    rtn)
{
  rtn = ++_ALLOC

  Srcs[rtn] = rtn
  SrcTarget[rtn] = target
  SrcPath[rtn] = path
  SrcName[rtn] = base_name(path)
  SrcObjDir[rtn] = TargetObjDir[target]
  SrcObjPath[rtn] = SrcObjDir[rtn] PATH_SEP SrcName[rtn]
  SrcObjPath[rtn] = path_head(SrcObjPath[rtn]) OBJ_SUFFIX
  SrcIsCxx[rtn] = 0

  if(path_suffix(path) == ".cpp")
  {
    SrcIsCxx[rtn] = 1
  }

  SrcScanned[rtn] = 0

  SrcScan(rtn)

  return rtn
}

function SrcScan(ctx,    h, p, rtn, file, line, type, dep, bn)
{
  if(SrcScanned[ctx])
  {
    return;
  }

  SrcScanned[ctx] = 1
  print("Scanning: " SrcPath[ctx])

  file = SrcPath[ctx]

  while(getline line < file > 0)
  {
    line = strip_whitespace(line)
    line = chomp_to(line, "#include")
    if(length(line) < 1) continue
    type = TYPE_RELATIVE

    if(substr(line, 1, 1) == "<")
    {
      type = TYPE_SYSTEM
    }

    line = substr(line, 2)
    line = substr(line, 1, length(line) - 1)
    bn = base_name(line)

    if(type == TYPE_RELATIVE)
    {
      for(h in Headers)
      {
        if(SrcName[h] != bn) continue
        if(SrcTarget[h] != SrcTarget[ctx]) continue
        dep = Dependency(ctx, type, h)
        SrcScan(h)
      }
    }
    else
    {
      for(h in Headers)
      {
        if(SrcName[h] != bn) continue
        dep = Dependency(ctx, type, h)
        SrcScan(h)
      }
    }
  }

  close(file)
}

############################################################################
# Header
############################################################################

function Header(target, path,    rtn)
{
  rtn = ++_ALLOC

  Headers[rtn] = rtn
  SrcTarget[rtn] = target
  SrcPath[rtn] = path
  SrcName[rtn] = base_name(path)
  SrcScanned[rtn] = 0

  return rtn;
}

############################################################################
# Dependency
############################################################################

function Dependency(parent, type, header,    rtn)
{
  rtn = ++_ALLOC

  Dependencies[rtn] = rtn
  DependencyParent[rtn] = parent
  DependencyType[rtn] = type
  DependencyHeader[rtn] = header

  return rtn
}

function DependencyFlatten(parent, out, outLibs,    h, t, dep)
{
  for(dep in Dependencies)
  {
    if(DependencyParent[dep] != parent) continue

    h = DependencyHeader[dep]
    t = SrcTarget[h]

    if(TargetType[t] == TYPE_LIBRARY)
    {
      outLibs[t] = 1
    }

    # Prevent infinite recursion if two dependencies depend on one another
    if(!out[SrcPath[h]])
    {
      out[SrcPath[h]] = 1
      DependencyFlatten(h, out, outLibs)
    }
  }
}

function DependencySerialize(parent,    f, dep, rtn, sep)
{
  DependencyFlatten(parent, f)
  rtn = ""
  sep = ""

  for(dep in f)
  {
    rtn = rtn sep dep
    sep = " "
  }

  return rtn
}

function TargetSerializeLibs(target, linker,    h, f, libs, lib, dep, rtn, sep)
{
  rtn = ""
  sep = ""

  for(dep in Dependencies)
  {
    h = DependencyParent[dep]
    if(SrcTarget[h] != target) continue
    DependencyFlatten(h, f, libs)
  }

  for(lib in libs)
  {
    if(TargetName[lib] == TargetName[target]) continue

    if(linker)
    {
      if(SYSTEM == SYS_POSIX)
      {
        rtn = rtn sep "-l" TargetName[lib]
      }
      else if(SYSTEM == SYS_WIN32)
      {
        rtn = rtn sep TargetName[lib] LIB_SUFFIX
      }
      else
      {
        error("Invalid platform")
      }
    }
    else
    {
      rtn = rtn sep TargetPath[lib]
    }

    sep = " "
  }

  return rtn
}

############################################################################
# Target
############################################################################

function Target(name,    rtn)
{
  rtn = ++_ALLOC

  Targets[rtn] = rtn
  TargetName[rtn] = name
  TargetObjDir[rtn] = "objs" PATH_SEP name

  TargetSetType(rtn, TYPE_LIBRARY)

  return rtn
}

function TargetSetType(ctx, type)
{
  TargetType[ctx] = type

  if(type == TYPE_EXECUTABLE)
  {
    TargetPath[ctx] = "bin" PATH_SEP TargetName[ctx] BIN_SUFFIX
  }
  else
  {
    TargetPath[ctx] = "lib" PATH_SEP LIB_PREFIX TargetName[ctx] LIB_SUFFIX
  }
}

############################################################################
# Makefile
############################################################################

function MakefileOutput(    t, s, f)
{
  f = "Makefile"

  if(SYSTEM == SYS_POSIX)
  {
    fwritel(f, "CXX=c++")
    fwritel(f, "CC=cc")
    fwritel(f, "AR=ar")
    fwritel(f, "CFLAGS=-g -Isrc -I/usr/X11R6/include -I/usr/X11R6/include/libdrm")
    fwritel(f, "CXXFLAGS=-Isrc")
    fwritel(f, "LFLAGS=-g -Llib -L/usr/X11R6/lib -ldrm")
    fwritel(f, "")
    fwritel(f, ".POSIX:")
    fwritel(f, ".PHONY: clean")
    fwritel(f, ".SUFFIXES:")
    fwritel(f, ".SUFFIXES: " OBJ_SUFFIX " .c .cpp")
  }
  else if(SYSTEM == SYS_WIN32)
  {
    fwritel(f, "CXX=cl /nologo")
    fwritel(f, "CC=cl /nologo")
    fwritel(f, "LIBTOOL=lib /NOLOGO")
    fwritel(f, "LINK=link /NOLOGO")
    fwritel(f, "CXXFLAGS=/EHsc /Isrc")
    fwritel(f, "CFLAGS=/Isrc")
    fwritel(f, "LFLAGS=/LIBPATH:lib")
    fwritel(f, "")
    fwritel(f, ".SUFFIXES:")
    fwritel(f, ".SUFFIXES: " OBJ_SUFFIX " .c .cpp")
  }
  else
  {
    error("Invalid system specified")
  }

  fwritel(f, "")

  ## All ##

  fwrite(f, "all: objs" PATH_SEP ".dirs")

  for(t in Targets)
  {
    if(TargetType[t] != TYPE_LIBRARY) continue
    fwrite(f, " " TargetPath[t])
  }

  for(t in Targets)
  {
    if(TargetType[t] != TYPE_EXECUTABLE) continue
    fwrite(f, " " TargetPath[t])
  }

  fwritel(f, "")
  fwritel(f, "")

  ## Target ##

  for(t in Targets)
  {
    fwrite(f, TargetPath[t] ":")

    for(s in Srcs)
    {
      if(SrcTarget[s] != t) continue
      fwrite(f, " " SrcObjPath[s])
    }

    fwrite(f, " " TargetSerializeLibs(t, 0))

    fwritel(f, "")
    fwrite(f, "\t")

    if(TargetType[t] == TYPE_EXECUTABLE)
    {
      if(SYSTEM == SYS_POSIX)
      {
        fwrite(f, "$(CXX) -o$@")
      }
      else if(SYSTEM == SYS_WIN32)
      {
        fwrite(f, "$(LINK) /OUT:$@")
      }
      else
      {
        error("Invalid platform");
      }
    }
    else
    {
      if(SYSTEM == SYS_POSIX)
      {
        fwrite(f, "$(AR) rcs $@")
      }
      else if(SYSTEM == SYS_WIN32)
      {
        fwrite(f, "$(LIBTOOL) /OUT:$@")
      }
      else
      {
        error("Invalid platform");
      }
    }

    for(s in Srcs)
    {
      if(SrcTarget[s] != t) continue
      fwrite(f, " " SrcObjPath[s])
    }

    if(TargetType[t] == TYPE_EXECUTABLE)
    {
      fwrite(f, " $(LFLAGS) ")
      fwrite(f, TargetSerializeLibs(t, 1))
    }

    fwritel(f, "")
    fwritel(f, "")
  }

  ## Obj ##

  for(s in Srcs)
  {
    fwrite(f, SrcObjPath[s] ": " SrcPath[s] " " DependencySerialize(s))
    fwritel(f, "")

    if(SrcIsCxx[s])
    {
      fwrite(f, "\t$(CXX) $(CXXFLAGS)")
    }
    else
    {
      fwrite(f, "\t$(CC) $(CFLAGS)")
    }

    if(SYSTEM == SYS_POSIX)
    {
      fwrite(f, " -c -o$@ " SrcPath[s])
    }
    else if(SYSTEM == SYS_WIN32)
    {
      fwrite(f, " /c /Fo$@ " SrcPath[s])
    }
    else
    {
      error("Invalid platform")
    }

    fwritel(f, "")

    fwritel(f, "")
  }

  ## Dirs ##

  fwritel(f, "objs" PATH_SEP ".dirs:")
  fwritel(f, "\tmkdir bin")
  fwritel(f, "\tmkdir lib")
  fwritel(f, "\tmkdir objs")

  for(t in Targets)
  {
    fwritel(f, "\tmkdir " TargetObjDir[t])
  }

  if(SYSTEM == SYS_POSIX)
  {
    fwritel(f, "\t@touch $@")
  }
  else if(SYSTEM == SYS_WIN32)
  {
    fwritel(f, "\t@echo done > $@")
  }
  else
  {
    error("Invalid platform")
  }

  fwritel(f, "")

  ## Clean ##

  fwritel(f, "clean:")

  for(s in Srcs)
  {
    if(SYSTEM == SYS_POSIX)
    {
      #fwritel(f, "\trm -f " SrcObjPath[s])
    }
    else if(SYSTEM == SYS_WIN32)
    {
      #fwritel(f, "\t-del " SrcObjPath[s] " 2>NUL")
    }
    else
    {
      error("Invalid platform")
    }
  }

  if(SYSTEM == SYS_POSIX)
  {
    fwritel(f, "\trm -r -f bin")
    fwritel(f, "\trm -r -f lib")
    fwritel(f, "\trm -r -f objs")
  }
  else if(SYSTEM == SYS_WIN32)
  {
    fwritel(f, "\t-rmdir /s /q bin 2>NUL")
    fwritel(f, "\t-rmdir /s /q lib 2>NUL")
    fwritel(f, "\t-rmdir /s /q objs 2>NUL")
  }
  else
  {
    error("Invalid platform")
  }

  fwritel(f, "")

  close(f)
}

function fwritel(file, str)
{
  fwrite(file, str "\n")
}

function fwrite(file, str)
{
  printf(str) >file
}

############################################################################
# Filesystem utils
############################################################################

function path_head(path,    ci, rtn, ch)
{
  rtn = ""

  for(ci = 0; ci < length(path); ++ci)
  {
    ch = substr(path, ci + 1, 1)
    if(ch == ".") break
    rtn = rtn ch
  }

  return rtn
}

function base_name(path,    ci, ch)
{
  for(ci = length(path) - 1; ci >= 0; --ci)
  {
    ch = substr(path, ci + 1, 1)
    if(ch == "/" || ch == "\\") return substr(path, ci + 2)
  }

  return path
}

function path_suffix(path,    ci, ch)
{
  for(ci = length(path) - 1; ci >= 0; --ci)
  {
    ch = substr(path, ci + 1, 1)
    if(ch == ".") return substr(path, ci + 1)
  }

  return ""
}

function strip_whitespace(str,    ci, ch, rtn)
{
  rtn = ""

  for(ci = 0; ci < length(str); ++ci)
  {
    ch = substr(str, ci + 1, 1)

    if(ch != " " && ch != "\t")
    {
      rtn = rtn ch
    }
  }

  return rtn
}

function chomp_to(str, pat,    i)
{
  i = index(str, pat)

  if(!i) return ""

  return substr(str, i + length(pat))
}

function error(msg)
{
  print("Error: " msg)
  exit(1)
}

BEGIN { main() }

